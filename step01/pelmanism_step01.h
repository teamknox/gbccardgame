// Pelmanism for GBC
// 2001 TeamKNOx

// Display Information
#define MAX_TILE_NUMBER	128
#define DISPLAY_SIZE_X	 20
#define DISPLAY_SIZE_Y	 18

// Palette
#define CGBPal0c0 32767
#define CGBPal0c1 24311
#define CGBPal0c2 15855
#define CGBPal0c3 0

// Palette Setting
UWORD pBkgPalette[] = {
	CGBPal0c0, CGBPal0c1, CGBPal0c2, CGBPal0c3
};

// for general use

#define WORK_STR_LEN 20
char  gWorkStr[WORK_STR_LEN];
UWORD gWorkUW;
UBYTE gWorkUB;

