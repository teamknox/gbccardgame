// Pelmanism for GBC
// 2001 TeamKNOx

#include <gb.h>
#include <stdlib.h>
#include <string.h>

#include "pelmanism.h"

// BackGround
#include "pelmanism_bkg.c"

// Map coordinatetion
#include "pelmanism_map.c"

void init_disp()
{
  disable_interrupts();
  set_bkg_data( 0, MAX_TILE_NUMBER, pelmanism_bkg );
  SHOW_BKG;
  DISPLAY_ON;
  enable_interrupts();
  set_bkg_tiles( 0, 0, DISPLAY_SIZE_X, DISPLAY_SIZE_Y, pelmanism_map );
  set_bkg_palette( 0, 1, pBkgPalette );
}

int main()
{

	SPRITES_8x8;

	init_disp();

	return 0;

}
