// Pelmanism for GBC
// 2001 TeamKNOx

#include <gb.h>
#include <stdlib.h>
#include <string.h>

// STEP10
#include <rand.h>
// STEP10-END

#include "pelmanism.h"

// STEP09
#include "gb232.h"
#include "comm.h"
// STEP09-END

// BackGround
#include "pelmanism_bkg.c"

// STEP02
// object
#include "cursor.c"
// STEP02-END

// STEP06
#include "opening_map.c"
#include "completed_obj.c"
#include "gameover_obj.c"

// STEP06-END

// STEP08
#include "game_select_map.c"
#include "level_select_map.c"
// STEP08-END

// Map coordinatetion
#include "pelmanism_map.c"


// STEP09
void comm_init()
{
    set_gb232( SPEED_9600 | DATA_8 | STOP_1 | PARITY_NONE );
    init_gb232();
}


void sendStrings( char *strings )	
{
  while ( *strings ) {
	send_gb232( *strings );
	strings++;
  }
}
// STEP09-END


// STEP08
void bkgHandling()
{
	disable_interrupts();
	SHOW_BKG;
	DISPLAY_ON;
	enable_interrupts();
}
// STEP08-END

void init_disp()
{
  // STEP08
  /*
  disable_interrupts();
  set_bkg_data( 0, MAX_TILE_NUMBER, pelmanism_bkg );
  SHOW_BKG;
  DISPLAY_ON;
  enable_interrupts();
  */
  // STEP08-END
  set_bkg_tiles( 0, 0, DISPLAY_SIZE_X, DISPLAY_SIZE_Y, pelmanism_map );
  set_bkg_palette( 0, 1, pBkgPalette );
}

// STEP02
void set_cursor(UBYTE cursorType)
{
	UBYTE i;

	set_sprite_data(CURSOR_RELEASE, CURSOR_SIZE * CURSOR_NUMBER, cursor_data);
	for(i = 0;i <= CURSOR_SIZE;i++){
		set_sprite_tile(i, i + cursorType);
	}
	SHOW_SPRITES;
}

void move_cursor(UBYTE x, UBYTE y)
{
	move_sprite(0, x, y);
	move_sprite(1, x, y + CURSOR_MOVE_OFFSET);
	move_sprite(2, x + CURSOR_MOVE_OFFSET, y);
	move_sprite(3, x + CURSOR_MOVE_OFFSET, y + CURSOR_MOVE_OFFSET);
}
// STEP02-END

// STEP10
void getHexValue(UBYTE data)
{
	UBYTE i, work[2];
	
	work[0] = data >> 4;
	work[1] = data & 0x0F;

	for(i = 0;i <= 1;i++){
		gUWorkStr[i] = work[i] + '0';
		gWorkStr[i] = work[i] + '0';
		if(gWorkStr[i] > '9'){
			gWorkStr[i] = gWorkStr[i] + 7;
		}
	}
	gUWorkStr[2] = 0x00;
	gWorkStr[2] = 0x00;
}

void swapCard(UBYTE posX0, UBYTE posY0, UBYTE posX1, UBYTE posY1)
{
	UBYTE workCard;
	
	workCard = gCard[posY1][posX1];
	gCard[posY1][posX1] = gCard[posY0][posX0];
	gCard[posY0][posX0] = workCard;
}

// STEP11
void soundEffect(UBYTE num)
{
  NR50_REG = 0x77;
  NR51_REG = 0x11;
  NR52_REG = 0x8F;

  NR10_REG = sndPara[num][0];
  NR11_REG = 0x80;
  NR12_REG = 0xF0;
  NR13_REG = 0x00;
  NR14_REG = sndPara[num][1];
}
// STEP11-END



// STEP10-END

// STEP04
void shuffleCard()
{
	UBYTE i, j;
	
	// STEP10
	UBYTE cardNumber[SWAP_CARD_NO], cardType[SWAP_CARD_NO];
	UBYTE shuffleCount, byteLower, byteUpper;
	// STEP10-END
	
	// WorkArea clear
	for(j = 0; j < NUMBER_OF_TYPE;j++){
		for(i = 0; i < NUMBER_OF_CARD;i++){
			gCardWork[j][i] = 0;
		}
	}
	
	// Shuffle Engine
	for(j = 0; j < NUMBER_OF_TYPE;j++){
		for(i = 0; i < NUMBER_OF_CARD;i++){
			gCard[j][i] = i + NUMBER_OF_CARD * j;
		}
	}
	
	// STEP10
	initrand(sys_time);

	// DEBUG
	// Display sys_time value on terminal
	byteUpper = (UBYTE)(sys_time / 256);
	getHexValue(byteUpper);
	sendStrings(gWorkStr);

	byteLower = (UBYTE)(sys_time % 256);
	getHexValue(byteLower);
	sendStrings(gWorkStr);

	send_gb232(CR);
	send_gb232(LF);


	// Display shffleCount on terminal
	shuffleCount = rand();

	byteUpper = (UBYTE)(shuffleCount / 256);
	getHexValue(byteUpper);
	sendStrings(gWorkStr);

	byteLower = (UBYTE)(shuffleCount % 256);
	getHexValue(byteLower);
	sendStrings(gWorkStr);

	send_gb232(CR);
	send_gb232(LF);
	// DEBUG-END


	// Shuffling Card
	for(j = 0;j < shuffleCount;j++){
		for(i = 0;i < SWAP_CARD_NO;i++){
			cardNumber[i] = rand() % NUMBER_OF_CARD;
			cardType[i] = rand() % NUMBER_OF_TYPE;
		}
	
		swapCard(cardNumber[0], cardType[0], cardNumber[1], cardType[1]);
	}
	// STEP10-END
}

void dispCard(UBYTE orderCard)
{
	set_bkg_tiles(orderCard * CARD_ORDERING_POS, CARD_ELEMENTS_X, CARD_ELEMENTS_Y, tCardWork);
}

UBYTE makeCard(UBYTE cardID)
{
	UBYTE cardType, cardNumber;

	cardNumber = cardID % NUMBER_OF_CARD;
	cardType = cardID / NUMBER_OF_CARD;

	tCardWork[0] = SPADE_1_4 + cardType * 2;	//1st element
	tCardWork[1] = CARD_BLANK_1_4;

	tCardWork[2] = SPADE_2_4 + cardType * 2;	//2nd element
	tCardWork[3] = CARD_BLANK_2_4;
	
	tCardWork[4] = CARD_BLANK_3_4;				//3rd element
	tCardWork[5] = CARD_01_3_4 + cardNumber * 2;
	
	tCardWork[6] = CARD_BLANK_4_4;				//4th element
	tCardWork[7] = CARD_01_4_4 + cardNumber * 2;

	return cardNumber;
}

void clearCard()
{
	UBYTE i;

	for(i = 0;i < CARD_ELEMENTS;i++){
		tCardWork[i] = 0;
	}

	dispCard(0);
	dispCard(1);
}

// STEP04-END

// STEP08
void num02str(UBYTE number)

{
  UWORD m;
  UBYTE i, n, f;
  char work[4];

  f = 0; m = 10;
  for( i=0; i<2; i++ ) {
    n = number / m; number = number % m; m = m / 10;
    if( (n==0)&&(f==0) ) {
//      work[i] = ' ';		// ' '
      work[i] = '0';		// '0'
    } else {
      f = 1;
      work[i] = n + '0';	//'0' - '9'
    }
  }
  work[i] = 0x00;

  i = n = 0;
  while(work[n]){
  	while(work[n] == ' '){
  		n++;
  	}
  	gUWorkStr[i] = work[n];
  	gWorkStr[i] = work[n];
  	i++;
  	n++;
  }
  gUWorkStr[i] = 0x00;
  gWorkStr[i] = 0x00;
}


// STEP08

/*
// STEP05
// Number to Strings
void num04str(UWORD number)

{
  UWORD m;
  UBYTE i, n, f;
  char work[6];

  f = 0; m = 1000;
  for( i=0; i<4; i++ ) {
    n = number / m; number = number % m; m = m / 10;
    if( (n==0)&&(f==0) ) {
//      work[i] = ' ';		// ' '
      work[i] = '0';		// '0'
    } else {
      f = 1;
      work[i] = n + '0';	//'0' - '9'
    }
  }
  work[i] = 0x00;

  i = n = 0;
  while(work[n]){
  	while(work[n] == ' '){
  		n++;
  	}
  	gWorkStr[i] = work[n];
  	i++;
  	n++;
  }
  gWorkStr[i] = 0x00;
}

// STEP05-END
*/

// STEP08-END

// STEP06

void setOpening()
{
	UBYTE i;

	disable_interrupts();
	set_bkg_data( 0, MAX_TILE_NUMBER, pelmanism_bkg );
	SHOW_BKG;
	DISPLAY_ON;
	enable_interrupts();
    set_bkg_tiles( 0, 0, DISPLAY_SIZE_X, DISPLAY_SIZE_Y, opening_map );
	set_bkg_palette( 0, 1, pBkgPalette );
	// STEP11
	soundEffect(MATCHED_SOUND);	// dummy for just started
	// STEP11-END

	// CARD handling
	for(i = 0;i < NUMBER_OF_TYPE;i++){
		makeCard(SPADE + i * NUMBER_OF_CARD);
		set_bkg_tiles(OPENING_CARD_POS_X + i * OPENING_CARD_OFFSET, OPENING_CARD_POS_Y, CARD_ELEMENTS_X, CARD_ELEMENTS_Y, tCardWork);
		// STEP11
		soundEffect(MATCHED_SOUND);
		// STEP11-END
		delay(OPENING_CARD_TIME);
	}
	
	// Message "ANY KEY TO START"
	set_bkg_tiles(ANY_POS_X, ANY_POS_Y, ANY_LEN, MESSAGE_HIGHT, sAny);
	set_bkg_tiles(KEY_POS_X, ANY_POS_Y, KEY_LEN, MESSAGE_HIGHT, sKey);
	set_bkg_tiles(TO_POS_X, ANY_POS_Y, TO_LEN, MESSAGE_HIGHT, sTo);
	set_bkg_tiles(START_POS_X, ANY_POS_Y, START_LEN, MESSAGE_HIGHT, sStart);
}


// This funcition is used for message display.
// "COMPLETED !!" and " GAME OVER" will be appeared on main pane.
void dispObjMessage(UBYTE objPos, UBYTE objLen, UBYTE posX, UBYTE posY, UBYTE objOffset, unsigned char *objName)
{
	UBYTE i;

	set_sprite_data(objPos, objLen, objName);
	for(i = 0;i < objLen;i++){
		set_sprite_tile(i + objPos, i + objPos);
		move_sprite(i + objPos, posX + i * objOffset, posY);
	}
	SHOW_SPRITES;
}

// STEP06-END


// STEP09
void sendShuffle()
{
	UBYTE i, j;

	// Send Shuffle result
	for(j = 0; j < NUMBER_OF_TYPE;j++){
		send_gb232(CR);
		send_gb232(LF);
		for(i = 0; i < NUMBER_OF_CARD;i++){
			num02str(gCard[j][i]);
			sendStrings(gWorkStr);
			send_gb232(' ');
		}
	}
	send_gb232(CR);
	send_gb232(LF);
}
// STEP09-END


// STEP07
UBYTE pelemanism()
{
	// STEP02
	UBYTE key1, key2, i, j, pos_x, pos_y;
	// STEP02-END

	// STEP03
	UBYTE flipCounter, flipPosX[NUMBER_OF_FLIP], flipPosY[NUMBER_OF_FLIP];
	// STEP03-END

	// STEP05
	UBYTE flipedCard[NUMBER_OF_FLIP];
//	UWORD clickCounter, leftCard;
	UBYTE clickCounter, leftCard;
	// STEP05-END

	// STEP07
	UBYTE loopControl;
	// STEP07-END

    // STEP08
    UBYTE flipedCardID[NUMBER_OF_FLIP];
    // STEP08-END

	// STEP07
	loopControl = 1;
	// STEP08
//	clickCounter = 0UL;
//	flipCounter = 0UL;
	clickCounter = 0U;
	flipCounter = 0U;
	// STEP08-END
	// STEP07-END

    // STEP08
	// STEP04
	shuffleCard();
	// STEP09
	sendShuffle();
	// STEP09-END
	// STEP04-END
	// STEP08-END

    // STEP08
    bkgHandling();
    // STEP08-END

	init_disp();
	
	// STEP08
	switch(gSelectGame){
		case SELECT_GAME_MARKY:
			set_bkg_tiles(MARKY_POS_X, MARKY_POS_Y, MARKY_LEN, MESSAGE_HIGHT, sMarky);
		break;
		case SELECT_GAME_PELMANISM:
			set_bkg_tiles(PELMANISM_POS_X, PELMANISM_POS_Y, PELMANISM_LEN, MESSAGE_HIGHT, sPelmanism);
		break;
		case SELECT_GAME_FLASHER:
			set_bkg_tiles(FLASHER_POS_X, FLASHER_POS_Y, FLASHER_LEN, MESSAGE_HIGHT, sFlasher);
		break;
	}
	// STEP08-END

	// STEP05
	leftCard = NUMBER_OF_TOTAL;
	// STEP05-END

	// STEP02
	// Cursor Handling
	i = j = 0;
	// STEP02-END

	// Cursor move
	set_cursor(CURSOR_RELEASE);
	move_cursor(i * KEY_STEP_X + START_CURSOR_X, j * KEY_STEP_Y + START_CURSOR_Y);		// cursor appear

	while(loopControl){
		delay(10);

		key1 = joypad();

		if(key1 != key2){
			pos_x = i * KEY_STEP_X + START_CURSOR_X;
			pos_y = j * KEY_STEP_Y + START_CURSOR_Y;
			move_cursor(pos_x, pos_y);
		}
		
		if(key2 & J_A){
			if(key1 & J_A){
				// Make continuslly
			}
			else{
				// Just released !!
				set_cursor(CURSOR_RELEASE);
			}
		}
		else{
			if(key1 & J_A){
				// Just Make !!
				// STEP11
				soundEffect(FLIP_SOUND);
				// STEP11-END
				// STEP04
				if(gCardWork[j][i] == 0){
				// STEP04-END
					set_cursor(CURSOR_PUSH);

					// STEP03
					// Flip Card
					if(flipCounter < NUMBER_OF_FLIP){
						set_bkg_tiles(i + CARD_POS_X, j * CARD_SIZE_Y + CARD_POS_Y, CARD_SIZE_X, CARD_SIZE_Y, tCardReverse);
						// STEP04
						// makeCard(gCard[j][i]);
						// STEP05
						if(clickCounter == GAME_OVER_COUNT){
							// Game Over
							gMode = MODE_GAMEOVER;
							loopControl = 0;
						}
						else{
							clickCounter++;
							// STEP08
						//	num04str(clickCounter);
						    num02str(clickCounter);
						    // STEP08-END
						    // STEP09
//							set_bkg_tiles(COUNT_DISP_POS, NUMBER_OF_DIGIT, gWorkStr);
							set_bkg_tiles(COUNT_DISP_POS, NUMBER_OF_DIGIT, gUWorkStr);
							// STEP09-END

                            // STEP08
                            flipedCardID[flipCounter] = gCard[j][i];
                            // STEP08-END
							flipedCard[flipCounter] = makeCard(gCard[j][i]);
							// STEP05-END
							// STEP08
							if((gSelectGame != SELECT_GAME_FLASHER)){
							    dispCard(flipCounter);
							}
							// STEP08-END
							flipPosX[flipCounter] = i;
							flipPosY[flipCounter] = j;
							flipCounter++;
							gCardWork[j][i] = flipCounter;
							// STEP04-END
						}
					}
					// STEP03-END
				}
			}
			else{
				// STEP03
				// Restore Card
//				if(flipCounter >= NUMBER_OF_FLIP){
				// STEP05
				if(flipCounter == NUMBER_OF_FLIP){
				// STEP05-END
			        // delay(NORMAL_TIME);
				    // STEP08
				    if(gSelectGame == SELECT_GAME_FLASHER){
        			    makeCard(flipedCardID[0]);
		        	    dispCard(0);
				        makeCard(flipedCardID[1]);
				        dispCard(1);
		        	}
				    delay(gSelectTime);
					// STEP08-END
					// STEP05
					if(flipedCard[0] == flipedCard[1]){
						// STEP11
						soundEffect(MATCHED_SOUND);
						// STEP11-END
						// STEP06
						leftCard = leftCard - CARD_PAIR;
						// STEP08
						// num04str(leftCard);
						num02str(leftCard);
						// STEP08-END
						// STEP09
//						set_bkg_tiles(LEFT_DISP_POS, NUMBER_OF_DIGIT, gWorkStr);
						set_bkg_tiles(LEFT_DISP_POS, NUMBER_OF_DIGIT, gUWorkStr);
						// STEP09-END

						// Remove matched card
						set_bkg_tiles(flipPosX[0] + CARD_POS_X, flipPosY[0] * CARD_SIZE_Y + CARD_POS_Y, CARD_SIZE_X, CARD_SIZE_Y, tRemovedCard);
						set_bkg_tiles(flipPosX[1] + CARD_POS_X, flipPosY[1] * CARD_SIZE_Y + CARD_POS_Y, CARD_SIZE_X, CARD_SIZE_Y, tRemovedCard);
						gCardWork[flipPosY[0]][flipPosX[0]] = REMOVE_MARK;
						gCardWork[flipPosY[1]][flipPosX[1]] = REMOVE_MARK;
						// STEP06-END
						if(leftCard){
						}
						else{
							// Completed
							gMode = MODE_COMPLETED;
							loopControl = 0;
						}
					}
					else{
						// STEP11
						soundEffect(UNMATCHED_SOUND);
						// STEP11-END
						// STEP04
						// STEP08
						if(gSelectGame){
						    set_bkg_tiles(flipPosX[0] + CARD_POS_X, flipPosY[0] * CARD_SIZE_Y + CARD_POS_Y, CARD_SIZE_X, CARD_SIZE_Y, tCardFront);
						    set_bkg_tiles(flipPosX[1] + CARD_POS_X, flipPosY[1] * CARD_SIZE_Y + CARD_POS_Y, CARD_SIZE_X, CARD_SIZE_Y, tCardFront);
						}
						else{
						    set_bkg_tiles(flipPosX[0] + CARD_POS_X, flipPosY[0] * CARD_SIZE_Y + CARD_POS_Y, CARD_SIZE_X, CARD_SIZE_Y, tCardMarky);
						    set_bkg_tiles(flipPosX[1] + CARD_POS_X, flipPosY[1] * CARD_SIZE_Y + CARD_POS_Y, CARD_SIZE_X, CARD_SIZE_Y, tCardMarky);
                        }
						// STEP08-END
						gCardWork[flipPosY[0]][flipPosX[0]] = 0;
						gCardWork[flipPosY[1]][flipPosX[1]] = 0;
						gCardWork[j][i] = 0;
						// STEP04-END
					}
					// STEP05-END
					clearCard();
					flipCounter = 0;
				}
				// STEP03-END
			}
		}
		
		if(!(key1 & J_A)){
			if((key1 & J_UP) && !(key2 & J_UP) && j > 0)
				j--;
			else if((key1 & J_DOWN) && !(key2 & J_DOWN) && j < (UNIT_SIZE_Y - 1))
				j++;

			if((key1 & J_LEFT) && !(key2 & J_LEFT) && i > 0)
				i--;
			else if((key1 & J_RIGHT) && !(key2 & J_RIGHT) && i < (UNIT_SIZE_X - 1))
				i++;
		}
		key2 = key1;
	}
	// STEP02-END

	return 0;
}

// STEP07-END


// STEP08
UBYTE joypad_A_up()
{
	UBYTE key1, key2;

	while(1){
		delay(10);
		key1 = joypad();
		if(key2 & J_A){
			if(key1 & J_A){
				// Make continuslly
			}
			else{
				// Just released !!
				return 1;
			}
		}
		else{
			if(key1 & J_A){
				// Just Make !!
			}
		}
		
		key2 = key1;
	}

	return 0;
}

void setGameSelect()
{
    set_bkg_tiles( 0, 0, DISPLAY_SIZE_X, DISPLAY_SIZE_Y, game_select_map );
}

void setLevelSelect()
{
    set_bkg_tiles( 0, 0, DISPLAY_SIZE_X, DISPLAY_SIZE_Y, level_select_map );
}

void drawSelectCursor(UBYTE posX, UBYTE cursorStartPos, UBYTE cursorSize, UBYTE command)
{
	UBYTE startPosY;

	startPosY = cursorStartPos * SELECT_NO_ELEMENTS + SELECT_MENU_POS_OFFSET;

	if(command){
		// Draw Upper side
		set_bkg_tiles(posX, startPosY, FRAME_TILE_WIDTH, FRAME_TILE_HIGHT, tLeftUpperCorner);
		set_bkg_tiles(posX + FRAME_TILE_WIDTH, startPosY, cursorSize - FRAME_TILE_WIDTH, FRAME_TILE_HIGHT, tLine);
		set_bkg_tiles(posX + cursorSize - FRAME_TILE_WIDTH, startPosY, FRAME_TILE_WIDTH, FRAME_TILE_HIGHT, tRightUpperCorner);

		// Draw Mid side
		set_bkg_tiles(posX, startPosY + FRAME_TILE_HIGHT, FRAME_TILE_WIDTH, FRAME_TILE_HIGHT, tMidLine);
		set_bkg_tiles(posX + cursorSize - FRAME_TILE_WIDTH, startPosY + 1, FRAME_TILE_WIDTH, FRAME_TILE_HIGHT, tMidLine);

		// Draw Lower Side
		set_bkg_tiles(posX, startPosY + 2, FRAME_TILE_WIDTH, FRAME_TILE_HIGHT, tLeftLowerCorner);
		set_bkg_tiles(posX + FRAME_TILE_WIDTH, startPosY + 2, cursorSize - FRAME_TILE_WIDTH, FRAME_TILE_HIGHT, tLine);
		set_bkg_tiles(posX + cursorSize - FRAME_TILE_WIDTH, startPosY + 2, FRAME_TILE_WIDTH, FRAME_TILE_HIGHT, tRightLowerCorner);
	}
	else{
		set_bkg_tiles(posX, startPosY, cursorSize, FRAME_TILE_HIGHT, tClearCursor);

		set_bkg_tiles(posX, startPosY + FRAME_TILE_HIGHT, FRAME_TILE_WIDTH, FRAME_TILE_HIGHT, tClearCursor);
		set_bkg_tiles(posX + cursorSize - FRAME_TILE_WIDTH, startPosY + FRAME_TILE_HIGHT, FRAME_TILE_WIDTH, FRAME_TILE_HIGHT, tClearCursor);

		set_bkg_tiles(posX, startPosY + 2, cursorSize, FRAME_TILE_HIGHT, tClearCursor);
	}
}

UBYTE selectCursor(UBYTE posX, UBYTE cursorStartPos, UBYTE sizeCursor, UBYTE command)
{
	UBYTE j, key1, key2;

	j = cursorStartPos;
	// STEP10
//	drawSelectCursor(SELECT_GAME_POS_X, j, SELECT_GAME_WIDTH, CURSOR_ON);
	drawSelectCursor(posX, j, sizeCursor, CURSOR_ON);
	// STEP10-END

	while(1){
		delay(10);
		key1 = joypad();
		if(key1 != key2){
			// STEP10
//			drawSelectCursor(5, j, 11, 1);
			drawSelectCursor(posX, j, sizeCursor, CURSOR_ON);
			// STEP10-END
		}
		if(key2 & J_A){
			if(key1 & J_A){
				// Make continuslly
			}
			else{
				// Just released !!
				return j;
			}
		}
		else{
			if(key1 & J_A){
				// Just Make !!
			}
		}

		if((key1 & J_UP) && !(key2 & J_UP) && j > 0){
	        drawSelectCursor(posX, j, sizeCursor, CURSOR_OFF);
			j--;
		}
		else if((key1 & J_DOWN) && !(key2 & J_DOWN) && j < (CURSOR_NO - 1)){
	        drawSelectCursor(posX, j, sizeCursor, CURSOR_OFF);
			j++;
		}

		key2 = key1;
	}
	return j;
}

// STEP08-END

int main()
{
	// STEP09
	comm_init();
	// STEP09-END

	// STEP07
	gMode = MODE_OPENING;

	// STEP08
	gSelect = SELECT_GAME;
	// STEP10
//	gSelectGame = SELECT_GAME_FLASHER;
	gSelectGame = SELECT_GAME_PELMANISM;
	// STEP10-END
	gSelectLevel = SELECT_LEVEL_NORMAL;
	// STEP08-END

	SPRITES_8x8;

    // STEP08
	// STEP04
//	shuffleCard();
	// STEP04-END
	// STEP08-END

	while(1){
		switch(gMode){
			case MODE_GREETING:
			break;
			case MODE_OPENING:
				// STEP06
				setOpening();
//				while(!joypad());
//				gMode = MODE_GAME;
				// STEP08
				while(!joypad_A_up());
				gMode = MODE_SELECT;
				// STEP08-END
				// STEP06-END
			break;
			case MODE_SELECT:
				// STEP08
				// Select Game
                bkgHandling();
                setGameSelect();
				gSelectGame = selectCursor(SELECT_GAME_POS_X, gSelectGame, SELECT_GAME_WIDTH, CURSOR_ON);
				// STEP11
				soundEffect(UNMATCHED_SOUND);
				// STEP11-END

				// Select Level
                bkgHandling();
				setLevelSelect();
				gSelectLevel = selectCursor(SELECT_LEVEL_POS_X, gSelectLevel, SELECT_LEVEL_WIDTH, CURSOR_ON);
                gSelectTime =  (SELECT_LEVEL_HARD - gSelectLevel + SELECT_LEVEL_NORMAL) * HARD_TIME;
				// STEP11
				soundEffect(UNMATCHED_SOUND);
				// STEP11-END
				gMode = MODE_GAME;
				// STEP08-END
			break;
			case MODE_GAME:
				// STEP07
				pelemanism();
				// STEP07-END
			break;
			case MODE_COMPLETED:
				// STEP06
				dispObjMessage(COMPLETED_OBJ_POS, COMPLETED_OBJ_LEN, COMPLETED_POS_X, COMPLETED_POS_Y, COMPLETED_OBJ_OFFSET, completed_obj);
				// STEP11
				soundEffect(COMPLETED_SOUND);
				// STEP11-END
				// STEP06-END
				// STEP07
				// STEP08
				while(!joypad_A_up());
//				while(!joypad());
				// STEP08-END
				// Move "COMPLETED !!" to out side of display :-<
				dispObjMessage(COMPLETED_OBJ_POS, COMPLETED_OBJ_LEN, HIDE_COMPLETED_OBJ_POS, HIDE_COMPLETED_OBJ_POS, COMPLETED_OBJ_OFFSET, completed_obj);
				HIDE_SPRITES;
				gMode = MODE_OPENING;
				// STEP07-END
			break;
			case MODE_GAMEOVER:
				// STEP06
				set_bkg_tiles(GAMEOVER_PANE_POS_X, GAMEOVER_PANE_POS_Y, GAMEOVER_PANE_SIZE_X, GAMEOVER_PANE_SIZE_Y, tGameOverPane);
				dispObjMessage(GAMEOVER_OBJ_POS, GAMEOVER_OBJ_LEN, GAMEOVER_POS_X, GAMEOVER_POS_Y, GAMEOVER_OBJ_OFFSET, gameover_obj);
				// STEP11
				soundEffect(GAME_OVER_SOUND);
				// STEP11-END
				// STEP06-END
				// STEP07
				// STEP08
				delay(PAUSE_GAMEOVER);
				while(!joypad_A_up());
//				while(!joypad());
				// STEP08-END
				// Move "GAMEOVER" to out side of display :-<
				dispObjMessage(GAMEOVER_OBJ_POS, GAMEOVER_OBJ_LEN, HIDE_GAMEOVER_OBJ_POS, HIDE_GAMEOVER_OBJ_POS, GAMEOVER_OBJ_OFFSET, gameover_obj);
				HIDE_SPRITES;
				gMode = MODE_OPENING;
				// STEP07-END
			break;
		}
	}
	
	return 0;
	
	//STEP07-END

}
