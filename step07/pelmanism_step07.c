// Pelmanism for GBC
// 2001 TeamKNOx

#include <gb.h>
#include <stdlib.h>
#include <string.h>

//#include "pelmanism.h"
#include ".\\pelmanism_stepXX\\step07\\pelmanism_step07.h"

// BackGround
#include "pelmanism_bkg.c"

// STEP02
// object
#include "cursor.c"
// STEP02-END

// STEP06
#include "opening_map.c"
#include "completed_obj.c"
#include "gameover_obj.c"

// STEP06-END

// Map coordinatetion
#include "pelmanism_map.c"

void init_disp()
{
  disable_interrupts();
  set_bkg_data( 0, MAX_TILE_NUMBER, pelmanism_bkg );
  SHOW_BKG;
  DISPLAY_ON;
  enable_interrupts();
  set_bkg_tiles( 0, 0, DISPLAY_SIZE_X, DISPLAY_SIZE_Y, pelmanism_map );
  set_bkg_palette( 0, 1, pBkgPalette );
}

// STEP02
void set_cursor(UBYTE cursorType)
{
	UBYTE i;

	set_sprite_data(CURSOR_RELEASE, CURSOR_SIZE * CURSOR_NUMBER, cursor_data);
	for(i = 0;i <= CURSOR_SIZE;i++){
		set_sprite_tile(i, i + cursorType);
	}
	SHOW_SPRITES;
}

void move_cursor(UBYTE x, UBYTE y)
{
	move_sprite(0, x, y);
	move_sprite(1, x, y + CURSOR_MOVE_OFFSET);
	move_sprite(2, x + CURSOR_MOVE_OFFSET, y);
	move_sprite(3, x + CURSOR_MOVE_OFFSET, y + CURSOR_MOVE_OFFSET);
}
// STEP02-END

// STEP04
void shuffleCard()
{
	UBYTE i, j;
	for(j = 0; j < NUMBER_OF_TYPE;j++){
		for(i = 0; i < NUMBER_OF_CARD;i++){
			gCard[j][i] = i + NUMBER_OF_CARD * j;
			gCardWork[j][i] = 0;
		}
	}
}

void dispCard(UBYTE orderCard)
{
	set_bkg_tiles(orderCard * CARD_ORDERING_POS, CARD_ELEMENTS_X, CARD_ELEMENTS_Y, tCardWork);
}

UBYTE makeCard(UBYTE cardID)
{
	UBYTE cardType, cardNumber;

	cardNumber = cardID % NUMBER_OF_CARD;
	cardType = cardID / NUMBER_OF_CARD;

	tCardWork[0] = SPADE_1_4 + cardType * 2;	//1st element
	tCardWork[1] = CARD_BLANK_1_4;

	tCardWork[2] = SPADE_2_4 + cardType * 2;	//2nd element
	tCardWork[3] = CARD_BLANK_2_4;
	
	tCardWork[4] = CARD_BLANK_3_4;				//3rd element
	tCardWork[5] = CARD_01_3_4 + cardNumber * 2;
	
	tCardWork[6] = CARD_BLANK_4_4;				//4th element
	tCardWork[7] = CARD_01_4_4 + cardNumber * 2;

	return cardNumber;
}

void clearCard()
{
	UBYTE i;

	for(i = 0;i < CARD_ELEMENTS;i++){
		tCardWork[i] = 0;
	}

	dispCard(0);
	dispCard(1);
}

// STEP04-END


// STEP05
// Number to Strings
void num04str(UWORD number)

{
  UWORD m;
  UBYTE i, n, f;
  char work[6];

  f = 0; m = 1000;
  for( i=0; i<4; i++ ) {
    n = number / m; number = number % m; m = m / 10;
    if( (n==0)&&(f==0) ) {
//      work[i] = ' ';		// ' '
      work[i] = '0';		// '0'
    } else {
      f = 1;
      work[i] = n + '0';	//'0' - '9'
    }
  }
  work[i] = 0x00;

  i = n = 0;
  while(work[n]){
  	while(work[n] == ' '){
  		n++;
  	}
  	gWorkStr[i] = work[n];
  	i++;
  	n++;
  }
  gWorkStr[i] = 0x00;
}

// STEP05-END

// STEP06

void setOpening()
{
	UBYTE i;

	disable_interrupts();
	set_bkg_data( 0, MAX_TILE_NUMBER, pelmanism_bkg );
	SHOW_BKG;
	DISPLAY_ON;
	enable_interrupts();
    set_bkg_tiles( 0, 0, DISPLAY_SIZE_X, DISPLAY_SIZE_Y, opening_map );
	set_bkg_palette( 0, 1, pBkgPalette );

	// CARD handling
	for(i = 0;i < NUMBER_OF_TYPE;i++){
		makeCard(SPADE + i * NUMBER_OF_CARD);
		set_bkg_tiles(OPENING_CARD_POS_X + i * OPENING_CARD_OFFSET, OPENING_CARD_POS_Y, CARD_ELEMENTS_X, CARD_ELEMENTS_Y, tCardWork);
		delay(OPENING_CARD_TIME);
	}
	
	// Message "ANY KEY TO START"
	set_bkg_tiles(ANY_POS_X, ANY_POS_Y, ANY_LEN, MESSAGE_HIGHT, sAny);
	set_bkg_tiles(KEY_POS_X, ANY_POS_Y, KEY_LEN, MESSAGE_HIGHT, sKey);
	set_bkg_tiles(TO_POS_X, ANY_POS_Y, TO_LEN, MESSAGE_HIGHT, sTo);
	set_bkg_tiles(START_POS_X, ANY_POS_Y, START_LEN, MESSAGE_HIGHT, sStart);
}


// This funcition is used for message display.
// "COMPLETED !!" and " GAME OVER" will be appeared on main pane.
void dispObjMessage(UBYTE objPos, UBYTE objLen, UBYTE posX, UBYTE posY, UBYTE objOffset, unsigned char *objName)
{
	UBYTE i;

	set_sprite_data(objPos, objLen, objName);
	for(i = 0;i < objLen;i++){
		set_sprite_tile(i + objPos, i + objPos);
		move_sprite(i + objPos, posX + i * objOffset, posY);
	}
	SHOW_SPRITES;
}

// STEP06-END

// STEP07
UBYTE pelemanism()
{
	// STEP02
	UBYTE key1, key2, i, j, pos_x, pos_y;
	// STEP02-END

	// STEP03
	UBYTE flipCounter, flipPosX[NUMBER_OF_FLIP], flipPosY[NUMBER_OF_FLIP];
	// STEP03-END

	// STEP05
	UBYTE flipedCard[NUMBER_OF_FLIP];
	UWORD clickCounter, leftCard;
	// STEP05-END

	// STEP07
	UBYTE loopControl;
	// STEP07-END

	// STEP07
	loopControl = 1;
	clickCounter = 0UL;
	flipCounter = 0UL;
	// STEP07-END

	init_disp();

	// STEP05
	leftCard = NUMBER_OF_TOTAL;
	// STEP05-END

	// STEP02
	// Cursor Handling
	i = j = 0;
	// STEP02-END

	// Cursor move
	set_cursor(CURSOR_RELEASE);
	move_cursor(i * KEY_STEP_X + START_CURSOR_X, j * KEY_STEP_Y + START_CURSOR_Y);		// cursor appear

	while(loopControl){
		delay(10);

		key1 = joypad();

		if(key1 != key2){
			pos_x = i * KEY_STEP_X + START_CURSOR_X;
			pos_y = j * KEY_STEP_Y + START_CURSOR_Y;
			move_cursor(pos_x, pos_y);
		}
		
		if(key2 & J_A){
			if(key1 & J_A){
				// Make continuslly
			}
			else{
				// Just released !!
				set_cursor(CURSOR_RELEASE);
			}
		}
		else{
			if(key1 & J_A){
				// Just Make !!
				// STEP04
				if(gCardWork[j][i] == 0){
				// STEP04-END
					set_cursor(CURSOR_PUSH);

					// STEP03
					// Flip Card
					if(flipCounter < NUMBER_OF_FLIP){
						set_bkg_tiles(i + CARD_POS_X, j * CARD_SIZE_Y + CARD_POS_Y, CARD_SIZE_X, CARD_SIZE_Y, tCardReverse);
						// STEP04
						// makeCard(gCard[j][i]);
						// STEP05
						if(clickCounter == GAME_OVER_COUNT){
							// Game Over
							gMode = MODE_GAMEOVER;
							loopControl = 0;
						}
						else{
							clickCounter++;
							num04str(clickCounter);
							set_bkg_tiles(COUNT_DISP_POS, NUMBER_OF_DIGIT, gWorkStr);

							flipedCard[flipCounter] = makeCard(gCard[j][i]);
							// STEP05-END
							dispCard(flipCounter);
							flipPosX[flipCounter] = i;
							flipPosY[flipCounter] = j;
							flipCounter++;
							gCardWork[j][i] = flipCounter;
							// STEP04-END
						}
					}
					// STEP03-END
				}
			}
			else{
				// STEP03
				// Restore Card
//				if(flipCounter >= NUMBER_OF_FLIP){
				// STEP05
				if(flipCounter == NUMBER_OF_FLIP){
				// STEP05-END
					delay(NORMAL_TIME);
					// STEP05
					if(flipedCard[0] == flipedCard[1]){
						// STEP06
						leftCard = leftCard - CARD_PAIR;
						num04str(leftCard);
						set_bkg_tiles(LEFT_DISP_POS, NUMBER_OF_DIGIT, gWorkStr);
					
						// Remove matched card
						set_bkg_tiles(flipPosX[0] + CARD_POS_X, flipPosY[0] * CARD_SIZE_Y + CARD_POS_Y, CARD_SIZE_X, CARD_SIZE_Y, tRemovedCard);
						set_bkg_tiles(flipPosX[1] + CARD_POS_X, flipPosY[1] * CARD_SIZE_Y + CARD_POS_Y, CARD_SIZE_X, CARD_SIZE_Y, tRemovedCard);
						gCardWork[flipPosY[0]][flipPosX[0]] = REMOVE_MARK;
						gCardWork[flipPosY[1]][flipPosX[1]] = REMOVE_MARK;
						// STEP06-END
						if(leftCard){
						}
						else{
							// Completed
							gMode = MODE_COMPLETED;
							loopControl = 0;
						}
					}
					else{
						// STEP04
						set_bkg_tiles(flipPosX[0] + CARD_POS_X, flipPosY[0] * CARD_SIZE_Y + CARD_POS_Y, CARD_SIZE_X, CARD_SIZE_Y, tCardFront);
						set_bkg_tiles(flipPosX[1] + CARD_POS_X, flipPosY[1] * CARD_SIZE_Y + CARD_POS_Y, CARD_SIZE_X, CARD_SIZE_Y, tCardFront);
						gCardWork[flipPosY[0]][flipPosX[0]] = 0;
						gCardWork[flipPosY[1]][flipPosX[1]] = 0;
						gCardWork[j][i] = 0;
						// STEP04-END
					}
					// STEP05-END
					clearCard();
					flipCounter = 0;
				}
				// STEP03-END
			}
		}
		
		if(!(key1 & J_A)){
			if((key1 & J_UP) && !(key2 & J_UP) && j > 0)
				j--;
			else if((key1 & J_DOWN) && !(key2 & J_DOWN) && j < (UNIT_SIZE_Y - 1))
				j++;

			if((key1 & J_LEFT) && !(key2 & J_LEFT) && i > 0)
				i--;
			else if((key1 & J_RIGHT) && !(key2 & J_RIGHT) && i < (UNIT_SIZE_X - 1))
				i++;
		}
		key2 = key1;
	}
	// STEP02-END

	return 0;
}

// STEP07-END


int main()
{
	// STEP07
	gMode = MODE_OPENING;

	SPRITES_8x8;

	// STEP04
	shuffleCard();
	// STEP04-END

	while(1){
		switch(gMode){
			case MODE_GREETING:
			break;
			case MODE_OPENING:
				// STEP06
				setOpening();
				while(!joypad());
				gMode = MODE_GAME;
				// STEP06-END
			break;
			case MODE_SELECT:
			break;
			case MODE_GAME:
				// STEP07
				pelemanism();
				// STEP07-END
			break;
			case MODE_COMPLETED:
				// STEP06
				dispObjMessage(COMPLETED_OBJ_POS, COMPLETED_OBJ_LEN, COMPLETED_POS_X, COMPLETED_POS_Y, COMPLETED_OBJ_OFFSET, completed_obj);
				// STEP06-END
				// STEP07
				while(!joypad());
				// Move "COMPLETED !!" to out side of display :-<
				dispObjMessage(COMPLETED_OBJ_POS, COMPLETED_OBJ_LEN, HIDE_COMPLETED_OBJ_POS, HIDE_COMPLETED_OBJ_POS, COMPLETED_OBJ_OFFSET, completed_obj);
				HIDE_SPRITES;
				gMode = MODE_OPENING;
				// STEP07-END
			break;
			case MODE_GAMEOVER:
				// STEP06
				set_bkg_tiles(GAMEOVER_PANE_POS_X, GAMEOVER_PANE_POS_Y, GAMEOVER_PANE_SIZE_X, GAMEOVER_PANE_SIZE_Y, tGameOverPane);
				dispObjMessage(GAMEOVER_OBJ_POS, GAMEOVER_OBJ_LEN, GAMEOVER_POS_X, GAMEOVER_POS_Y, GAMEOVER_OBJ_OFFSET, gameover_obj);
				// STEP06-END
				// STEP07
				while(!joypad());
				// Move "GAMEOVER" to out side of display :-<
				dispObjMessage(GAMEOVER_OBJ_POS, GAMEOVER_OBJ_LEN, HIDE_GAMEOVER_OBJ_POS, HIDE_GAMEOVER_OBJ_POS, GAMEOVER_OBJ_OFFSET, gameover_obj);
				HIDE_SPRITES;
				gMode = MODE_OPENING;
				// STEP07-END
			break;
		}
	}
	
	return 0;
	
	//STEP07-END

}
