// Pelmanism for GBC
// 2001 TeamKNOx

// Display Information
#define MAX_TILE_NUMBER	128
#define DISPLAY_SIZE_X	 20
#define DISPLAY_SIZE_Y	 18

// Palette
#define CGBPal0c0 32767
#define CGBPal0c1 24311
#define CGBPal0c2 15855
#define CGBPal0c3 0

// Palette Setting
UWORD pBkgPalette[] = {
	CGBPal0c0, CGBPal0c1, CGBPal0c2, CGBPal0c3
};

// for general use

#define WORK_STR_LEN 20
// STEP09
//unsigned char  gWorkStr[WORK_STR_LEN];
unsigned char  gUWorkStr[WORK_STR_LEN];
// STEP09-END
UWORD gWorkUW;
UBYTE gWorkUB;


//
// STEP02
//

// Cursor configuration
// Type of Cursors
#define CURSOR_RELEASE	0
#define CURSOR_PUSH		4

// Cusor
#define CURSOR_NUMBER	2
#define CURSOR_SIZE		4

#define CURSOR_MOVE_OFFSET	8


// Key configuration
#define KEY_STEP_X 8
#define KEY_STEP_Y 16
#define START_CURSOR_X 30
#define START_CURSOR_Y 40


// Function Key configuration
#define UNIT_SIZE_X 13
#define UNIT_SIZE_Y 4

//Behavior
#define KEY_MAKE_MAKE	1
#define KEY_MAKE_BREAK	2
#define KEY_BREAK_MAKE	3



//
// STEP03
//
#define NUMBER_OF_CARD	UNIT_SIZE_X
#define NUMBER_OF_TYPE	UNIT_SIZE_Y
#define NUMBER_OF_TOTAL	NUMBER_OF_CARD * NUMBER_OF_TYPE

UBYTE gCard[NUMBER_OF_TYPE][NUMBER_OF_CARD];

#define CARD_REVERSE 124
#define CARD_FRONT	126
unsigned char tCardFront[] = {
	CARD_FRONT,
	CARD_FRONT + 1
};

unsigned char tCardReverse[] = {
	CARD_REVERSE,
	CARD_REVERSE + 1
};

// CARD DEFINITION
#define CARD_POS_X	3
#define CARD_POS_Y	2

#define CARD_SIZE_X 1
#define CARD_SIZE_Y 2

#define NUMBER_OF_FLIP 2

#define NORMAL_TIME 2000

//
// STEP04
//

#define CARD_ELEMENTS_X 2
#define CARD_ELEMENTS_Y 4
#define CARD_ELEMENTS CARD_ELEMENTS_X * CARD_ELEMENTS_Y

unsigned char tCardWork[CARD_ELEMENTS];

#define SPADE 0
#define HEART 1
#define CLOVER 2
#define DIAMOND 3

#define CARD_BLANK_1_4 10
#define CARD_BLANK_2_4 11
#define CARD_BLANK_3_4 12
#define CARD_BLANK_4_4 13

#define SPADE_1_4 14
#define SPADE_2_4 15

#define CARD_01_3_4 22
#define CARD_01_4_4 23

#define CARD_ORDERING_POS 4 + 3, 11

// STEP05
#define REMOVE_MARK 5
// #define COUNT_DISP_POS	15,12
// #define LEFT_DISP_POS	15,15
// STEP08
#define COUNT_DISP_POS	17,12
#define LEFT_DISP_POS	17,15
// STEP08-END
#define NUMBER_OF_DIGIT	4,1

#define CARD_PAIR	2
#define GAME_OVER_COUNT 20

#define REMOVED_CARD 0
unsigned char tRemovedCard[] = {
	REMOVED_CARD,
	REMOVED_CARD + 1
};

// STEP06

// Opening
// CARD ordering
#define OPENING_CARD_POS_X 3
#define OPENING_CARD_POS_Y 7
#define OPENING_CARD_OFFSET 4
#define OPENING_CARD_TIME 500 // 500ms

// "HIT ANY KEY TO START"
#define SPACE_CHAR 1
#define MESSAGE_HIGHT 1

#define ANY_POS_X 2
#define ANY_POS_Y 13
#define ANY_LEN 3
unsigned char sAny[] = {"ANY"};

#define KEY_POS_X ANY_POS_X + ANY_LEN + SPACE_CHAR
#define KEY_LEN 3
unsigned char sKey[] = {"KEY"};

#define TO_POS_X KEY_POS_X + KEY_LEN + SPACE_CHAR
#define TO_LEN 2
unsigned char sTo[] = {"TO"};

#define START_POS_X TO_POS_X + TO_LEN + SPACE_CHAR
#define START_LEN 5
unsigned char sStart[] = {"START"};

// COMPLETED!! setting
#define COMPLETED_OBJ_POS CURSOR_SIZE * CURSOR_NUMBER
#define COMPLETED_OBJ_LEN 10

#define COMPLETED_POS_X 44
#define COMPLETED_POS_Y 64
#define COMPLETED_OBJ_OFFSET 10

#define HIDE_COMPLETED_OBJ_POS 200

// GameOver setting
#define GAMEOVER_OBJ_POS COMPLETED_OBJ_POS + COMPLETED_OBJ_LEN
#define GAMEOVER_OBJ_LEN 8

#define GAMEOVER_POS_X 44
#define GAMEOVER_POS_Y 61
#define GAMEOVER_OBJ_OFFSET 10

#define GAMEOVER_PANE_POS_X 4
#define GAMEOVER_PANE_POS_Y 5

#define GAMEOVER_PANE_SIZE_X 11
#define GAMEOVER_PANE_SIZE_Y 2

#define HIDE_GAMEOVER_OBJ_POS 200

unsigned char tGameOverPane[] = {
	REMOVED_CARD, REMOVED_CARD, REMOVED_CARD, REMOVED_CARD, 
	REMOVED_CARD, REMOVED_CARD, REMOVED_CARD, REMOVED_CARD, 
	REMOVED_CARD, REMOVED_CARD, REMOVED_CARD, REMOVED_CARD, 
	REMOVED_CARD, REMOVED_CARD, REMOVED_CARD, REMOVED_CARD, 
	REMOVED_CARD, REMOVED_CARD, REMOVED_CARD, REMOVED_CARD, 
	REMOVED_CARD, REMOVED_CARD
};

// STEP07
UBYTE gMode;

#define MODE_GREETING	0
#define MODE_OPENING	1
#define MODE_SELECT		2
#define MODE_GAME		3
#define MODE_COMPLETED	4
#define MODE_GAMEOVER	5

UBYTE gCardWork[NUMBER_OF_TYPE][NUMBER_OF_CARD];

// STEP08
UBYTE gSelect;
UBYTE gSelectGame;
UBYTE gSelectLevel;

#define SELECT_GAME		0
#define SELECT_LEVEL	1

#define SELECT_GAME_MARKY		0
#define SELECT_GAME_PELMANISM	1
#define SELECT_GAME_FLASHER		2

#define SELECT_GAME_POS_X 5
#define SELECT_GAME_WIDTH 11


#define SELECT_LEVEL_EASY	0
#define SELECT_LEVEL_NORMAL	1
#define SELECT_LEVEL_HARD	2

#define SELECT_LEVEL_POS_X 5
#define SELECT_LEVEL_WIDTH 9

#define SELECT_NO_ELEMENTS 3
#define SELECT_MENU_POS_OFFSET 5

#define CURSOR_ON 1
#define CURSOR_OFF 0

#define CURSOR_NO 3

#define FRAME_LEFT_UPPER 58
#define FRAME_LINE 59
#define FRAME_RIGHT_UPPER 60
#define FRAME_MID_LINE 61
#define FRAME_LEFT_LOWER 62
#define FRAME_RIGHT_LOWER 63

#define FRAME_TILE_WIDTH 1
#define FRAME_TILE_HIGHT 1

unsigned char tClearCursor[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
unsigned char tLeftUpperCorner[] = {FRAME_LEFT_UPPER};
unsigned char tLine[] = {FRAME_LINE, FRAME_LINE, FRAME_LINE, FRAME_LINE, FRAME_LINE, FRAME_LINE, FRAME_LINE, FRAME_LINE, FRAME_LINE};
unsigned char tRightUpperCorner[] = {FRAME_RIGHT_UPPER};
unsigned char tMidLine[] = {FRAME_MID_LINE};
unsigned char tLeftLowerCorner[] = {FRAME_LEFT_LOWER};
unsigned char tRightLowerCorner[] = {FRAME_RIGHT_LOWER};

UWORD gSelectTime;

// #define EASY_TIME 3000
#define HARD_TIME 1000

unsigned char tCardMarky[] = {
	CARD_REVERSE,
	CARD_FRONT + 1
};

#define MARKY_POS_X 7
#define MARKY_POS_Y 1
#define MARKY_LEN 5
unsigned char sMarky[] = {"MARKY"};

#define PELMANISM_POS_X 5
#define PELMANISM_POS_Y MARKY_POS_Y
#define PELMANISM_LEN 9
unsigned char sPelmanism[] = {"PELMANISM"};

#define FLASHER_POS_X 6
#define FLASHER_POS_Y MARKY_POS_Y
#define FLASHER_LEN 7
unsigned char sFlasher[] = {"FLASHER"};

#define PAUSE_GAMEOVER 2500

// STEP09
char gWorkStr[WORK_STR_LEN];

