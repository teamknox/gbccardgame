// Pelmanism for GBC
// 2001 TeamKNOx

// Display Information
#define MAX_TILE_NUMBER	128
#define DISPLAY_SIZE_X	 20
#define DISPLAY_SIZE_Y	 18

// Palette
#define CGBPal0c0 32767
#define CGBPal0c1 24311
#define CGBPal0c2 15855
#define CGBPal0c3 0

// Palette Setting
UWORD pBkgPalette[] = {
	CGBPal0c0, CGBPal0c1, CGBPal0c2, CGBPal0c3
};

// for general use

#define WORK_STR_LEN 20
char  gWorkStr[WORK_STR_LEN];
UWORD gWorkUW;
UBYTE gWorkUB;


//
// STEP02
//

// Cursor configuration
// Type of Cursors
#define CURSOR_RELEASE	0
#define CURSOR_PUSH		4

// Cusor
#define CURSOR_NUMBER	2
#define CURSOR_SIZE		4

#define CURSOR_MOVE_OFFSET	8


// Key configuration
#define KEY_STEP_X 8
#define KEY_STEP_Y 16
#define START_CURSOR_X 30
#define START_CURSOR_Y 40


// Function Key configuration
#define UNIT_SIZE_X 13
#define UNIT_SIZE_Y 4

//Behavior
#define KEY_MAKE_MAKE	1
#define KEY_MAKE_BREAK	2
#define KEY_BREAK_MAKE	3

