// Pelmanism for GBC
// 2001 TeamKNOx

#include <gb.h>
#include <stdlib.h>
#include <string.h>

#include "pelmanism.h"

// BackGround
#include "pelmanism_bkg.c"

// STEP02
// object
#include "cursor.c"
// STEP02-END

// Map coordinatetion
#include "pelmanism_map.c"

void init_disp()
{
  disable_interrupts();
  set_bkg_data( 0, MAX_TILE_NUMBER, pelmanism_bkg );
  SHOW_BKG;
  DISPLAY_ON;
  enable_interrupts();
  set_bkg_tiles( 0, 0, DISPLAY_SIZE_X, DISPLAY_SIZE_Y, pelmanism_map );
  set_bkg_palette( 0, 1, pBkgPalette );
}

// STEP02
void set_cursor(UBYTE cursorType)
{
	UBYTE i;

	set_sprite_data(CURSOR_RELEASE, CURSOR_SIZE * CURSOR_NUMBER, cursor_data);
	for(i = 0;i <= CURSOR_SIZE;i++){
		set_sprite_tile(i, i + cursorType);
	}
	SHOW_SPRITES;
}

void move_cursor(UBYTE x, UBYTE y)
{
	move_sprite(0, x, y);
	move_sprite(1, x, y + CURSOR_MOVE_OFFSET);
	move_sprite(2, x + CURSOR_MOVE_OFFSET, y);
	move_sprite(3, x + CURSOR_MOVE_OFFSET, y + CURSOR_MOVE_OFFSET);
}
// STEP02-END


int main()
{
	// STEP02
	UBYTE key1, key2, i, j, pos_x, pos_y;
	// STEP02-END

	// STEP03
	UBYTE flipCounter, flipPosX[NUMBER_OF_FLIP], flipPosY[NUMBER_OF_FLIP];
	// STEP03-END


	SPRITES_8x8;

	init_disp();

	// STEP02
	// Cursor Handling
	i = j = 0;
	// STEP02-END

	// Cursor move
	set_cursor(CURSOR_RELEASE);
	move_cursor(i * KEY_STEP_X + START_CURSOR_X, j * KEY_STEP_Y + START_CURSOR_Y);		// cursor appear

	while(1){
		delay(10);

		key1 = joypad();

		if(key1 != key2){
			pos_x = i * KEY_STEP_X + START_CURSOR_X;
			pos_y = j * KEY_STEP_Y + START_CURSOR_Y;
			move_cursor(pos_x, pos_y);
		}
		
		if(key2 & J_A){
			if(key1 & J_A){
				// Make continuslly
			}
			else{
				// Just released !!
				set_cursor(CURSOR_RELEASE);
			}
		}
		else{
			if(key1 & J_A){
				// Just Make !!
				set_cursor(CURSOR_PUSH);

				// STEP03
				// Flip Card
				if(flipCounter < NUMBER_OF_FLIP){
					set_bkg_tiles(i + CARD_POS_X, j * CARD_SIZE_Y + CARD_POS_Y, CARD_SIZE_X, CARD_SIZE_Y, tCardReverse);
					flipPosX[flipCounter] = i;
					flipPosY[flipCounter] = j;
					flipCounter++;
				}
				// STEP03-END
			}
			else{
				// STEP03
				// Restore Card
				if(flipCounter >= NUMBER_OF_FLIP){
					delay(2000);
					set_bkg_tiles(flipPosX[0] + CARD_POS_X, flipPosY[0] * CARD_SIZE_Y + CARD_POS_Y, CARD_SIZE_X, CARD_SIZE_Y, tCardFront);
					set_bkg_tiles(flipPosX[1] + CARD_POS_X, flipPosY[1] * CARD_SIZE_Y + CARD_POS_Y, CARD_SIZE_X, CARD_SIZE_Y, tCardFront);
					flipCounter = 0;
				}
				// STEP03-END
			}
		}
		
		if(!(key1 & J_A)){
			if((key1 & J_UP) && !(key2 & J_UP) && j > 0)
				j--;
			else if((key1 & J_DOWN) && !(key2 & J_DOWN) && j < (UNIT_SIZE_Y - 1))
				j++;

			if((key1 & J_LEFT) && !(key2 & J_LEFT) && i > 0)
				i--;
			else if((key1 & J_RIGHT) && !(key2 & J_RIGHT) && i < (UNIT_SIZE_X - 1))
				i++;
		}
		key2 = key1;
	}
	// STEP02-END

	return 0;

}
