// Pelmanism for GBC
// 2001 TeamKNOx

// Display Information
#define MAX_TILE_NUMBER	128
#define DISPLAY_SIZE_X	 20
#define DISPLAY_SIZE_Y	 18

// Palette
#define CGBPal0c0 32767
#define CGBPal0c1 24311
#define CGBPal0c2 15855
#define CGBPal0c3 0

// Palette Setting
UWORD pBkgPalette[] = {
	CGBPal0c0, CGBPal0c1, CGBPal0c2, CGBPal0c3
};

// for general use

#define WORK_STR_LEN 20
unsigned char  gWorkStr[WORK_STR_LEN];
UWORD gWorkUW;
UBYTE gWorkUB;


//
// STEP02
//

// Cursor configuration
// Type of Cursors
#define CURSOR_RELEASE	0
#define CURSOR_PUSH		4

// Cusor
#define CURSOR_NUMBER	2
#define CURSOR_SIZE		4

#define CURSOR_MOVE_OFFSET	8


// Key configuration
#define KEY_STEP_X 8
#define KEY_STEP_Y 16
#define START_CURSOR_X 30
#define START_CURSOR_Y 40


// Function Key configuration
#define UNIT_SIZE_X 13
#define UNIT_SIZE_Y 4

//Behavior
#define KEY_MAKE_MAKE	1
#define KEY_MAKE_BREAK	2
#define KEY_BREAK_MAKE	3



//
// STEP03
//
#define NUMBER_OF_CARD	UNIT_SIZE_X
#define NUMBER_OF_TYPE	UNIT_SIZE_Y
#define NUMBER_OF_TOTAL	NUMBER_OF_CARD * NUMBER_OF_TYPE

UBYTE gCard[NUMBER_OF_TYPE][NUMBER_OF_CARD];

#define CARD_REVERSE 124
#define CARD_FRONT	126
unsigned char tCardFront[] = {
	CARD_FRONT,
	CARD_FRONT + 1
};

unsigned char tCardReverse[] = {
	CARD_REVERSE,
	CARD_REVERSE + 1
};

// CARD DEFINITION
#define CARD_POS_X	3
#define CARD_POS_Y	2

#define CARD_SIZE_X 1
#define CARD_SIZE_Y 2

#define NUMBER_OF_FLIP 2

#define NORMAL_TIME 2000

//
// STEP04
//

#define CARD_ELEMENTS_X 2
#define CARD_ELEMENTS_Y 4
#define CARD_ELEMENTS CARD_ELEMENTS_X * CARD_ELEMENTS_Y

unsigned char tCardWork[CARD_ELEMENTS];

#define SPADE 0
#define HEART 1
#define CLOVER 2
#define DIAMOND 3

#define CARD_BLANK_1_4 10
#define CARD_BLANK_2_4 11
#define CARD_BLANK_3_4 12
#define CARD_BLANK_4_4 13

#define SPADE_1_4 14
#define SPADE_2_4 15

#define CARD_01_3_4 22
#define CARD_01_4_4 23

// STEP05
#define REMOVE_MARK 5
#define COUNT_DISP_POS	15,12
#define LEFT_DISP_POS	15,15
#define NUMBER_OF_DIGIT	4,1

#define CARD_PAIR	2
#define GAME_OVER_COUNT 99

#define REMOVED_CARD 0
unsigned char tRemovedCard[] = {
	REMOVED_CARD,
	REMOVED_CARD + 1
};

